<html>
<head>
<title>Merchant Admin</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
  <link rel="stylesheet" href="css/style.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>

  <script>
    
    $(document).ready(function(){

      $(".form-container").on("submit", function(){
        var range    = $("#range").val();
        var location = $("#location").val();
console.log("HERE");
        if( range.length <= 0 ){
          $(".range-error").html("Please enter valid range");
          return false ;
        }
      
        if( location.length <= 0 ){
          $(".location-error").html("Please enter valid location");
          return false ;
        }

        if( !range.match(/^\d+$/)){
          $(".range-error").html("Range should be only numeric");
          return false ;
        }			       

        $.ajax({
	  url : "/merchant/getmerchantIDs?range="+ range +"&location=" + location ,		       
        }).done(function(response){
	  console.log(response);
	$(".result-container").removeClass("hide");
    
	$(".result").html(response.join(", "));

        			       
    		       
        }).error(function(error){
        });


      });

    
    
    

    });

  </script>

</head>

<body>
  <div class="navbar-fixed">
	<h3 class="center cyan darken-3 white-text z-depth-1 margin0">Merchant Admin</h3>
  </div>        
  <div class="container white z-depth-1 main-container">
    <form class="form-container" name="merchant-form" action="javascript:void(0);">
      
      <h5 class="center grey-text text-darken-4"> Get Merchant IDs </h5>

      <div class="row">
      <div class="input-field col s6">
          <input placeholder="Enter Range" id="range" type="text" class="validate">
	  <div class="range-error red-text"></div>
      </div>      
      </div>
      
      <div class="row">
      <div class="input-field col s6">
          <input placeholder="Enter Location" id="location" type="text" class="validate">
	  <div class="location-error red-text"></div>
      </div>            
      </div>      

      <div class="row center">
	<button type="submit" class="btn btn-medium waves-effect waves-light" id="submit-btn">SUBMIT
            <i class="mdi-content-send right"></i>
	</button>     
      </div>
    </form>
    
    <div class="row hide result-container" style="padding: 30px;">
      <div >Response : </div>
      <div class="result"></div>
    </div>  
    
  </div>
</body>
</html>
