package com.merchant.servlet ;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;

import org.apache.jasper.runtime.JspWriterImpl;
import org.codehaus.jackson.map.ObjectMapper;



/**
 * @author ankita
 *
 */
public class GetMerchantIDByRange extends HttpServlet {
	
	private static final long serialVersionUID = 7391855708253966854L;
	
    public void init(ServletConfig servletConfig) throws ServletException {
	super.init(servletConfig);
	//
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

    	    String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    	    String DB_URL = "jdbc:mysql://127.0.0.1/merchant";
    	    String USER = "root";
    	    String PASS = "dost123";

    	   Connection conn = null;
    	   Statement stmt = null;
    	   
    		ObjectMapper mapper = new ObjectMapper();

	try {

		String range = request.getParameter("range");
		String location = request.getParameter("location");
		
	      Class.forName("com.mysql.jdbc.Driver");

	      conn = DriverManager.getConnection(DB_URL, USER, PASS);
	      stmt = conn.createStatement();

	      String merchantQuery = "select merchantID from merchantInfo where pin = " + range + "  and address like '% " + location +" %'";

	      ResultSet rs = stmt.executeQuery(merchantQuery);

	      List<String> ids = new ArrayList<String>();
	      while(rs.next()){
	         String name = rs.getString("merchantID");
	         ids.add(name);	
	      }

	      rs.close();

	      if(ids.size() <=0 ){
		  ids.add("No merchant ids found");
	      }
		
	      String output = mapper.writeValueAsString(ids);
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(output);

	} catch(SQLException se){
	      //Handle errors for JDBC
		System.out.println("Exception %s" + se.toString());
	}catch (Exception e) {
	    System.out.println("Exception %s" + e.toString());
	}

    }
	
}